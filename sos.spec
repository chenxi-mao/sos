%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:          sos
Version:       4.0
Release:       6
Summary:       A set of tools to gather troubleshooting information from a system
License:       GPLv2+
URL:           https://github.com/sosreport/sos
Source0:       https://github.com/sosreport/sos/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

Patch6000:     backport-Fix-dict-order-py38-incompatibility.patch
Patch9000:     openEuler-add-openEuler-policy.patch
Patch9001:     add-uniontech-os-support.patch
Patch9002:     Fix-sos-command-failed-in-sos-4.0.patch
Patch9003:     add-KylinSec-OS-support.patch

BuildRequires: python3-devel gettext
Requires:      libxml2-python3 bzip2 xz python3-rpm tar python3-pexpect
BuildArch:     noarch

%description
Sos is an extensible, portable, support data collection tool primarily
aimed at Linux distributions and other UNIX-like operating systems.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%py3_build

%install
%py3_install '--install-scripts=%{_sbindir}'

install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}
install -d -m 700 %{buildroot}%{_sysconfdir}/%{name}/cleaner
install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}/presets.d
install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}/groups.d
install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}/extras.d
install -m 644 %{name}.conf %{buildroot}%{_sysconfdir}/%{name}/%{name}.conf

%find_lang %{name} || echo 0

%files -f %{name}.lang
%license LICENSE
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/sos/sos.conf
%{_sbindir}/sos*
%dir /etc/sos/cleaner
%dir /etc/sos/presets.d
%dir /etc/sos/extras.d
%dir /etc/sos/groups.d
%{python3_sitelib}/*
%exclude %{_datadir}/doc/sos/{AUTHORS,README.md}

%files help
%defattr(-,root,root)
%doc AUTHORS README.md
%{_mandir}/man1/*
%{_mandir}/man5/*

%changelog
* Wed Feb 23 2022 liuxingxiang <liuxingxiang@kylinsec.com.cn> - 4.0-6
- add KylinSec policy

* Tue Feb 22 2022 weidong <weidong@uniontech.com> - 4.0-5
- Fix sos command failed in sos 4.0

* Mon Jul 19 2021 liugang <liuganga@uniontech.com> - 4.0-4
- add UnionTech policy

* Mon Mar 08 2021 shixuantong <shixuantong@huawei.com> - 4.0-3
- add openEuler policy

* Tue Mar 03 2021 shixuantong <shixuantong@huawei.com> - 4.0-2
- fix unable to read configure file /etc/sos/sos.conf issue

* Tue Feb 02 2021 shixuantong <shixuantong@huawei.com> - 4.0-1
- Upgrade to version 4.0

* Mon Jul 13 2020 Jeffery.Gao <gaojianxing@huawei.com> - 3.9.1-3
- Append openEuler policy

* Mon Jun 1 2020 chengzihan <chengzihan2@huawei.com> - 3.9.1-2
- Package update

* Mon Feb 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.6-5
- Package init
